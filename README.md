# SGPP - Simple Game Pairing Protocol #

This server was built as a bachelor graduation project to support multiplayer matchmaking on a mutliplatform game I was working on. Protocol documentation is available in italian only at the moment. Here is a list of the games I've built that use SGPP.

* [Spacenauts](https://github.com/alessiocali/spacenauts)   
	A multiplatform, old style vertical space shooter written using libGDX. Supported platforms include Desktop, Android and Web.