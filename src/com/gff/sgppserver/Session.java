package com.gff.sgppserver;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.UUID;

/**
 * <p>
 * Represents a matchmaking session. As such it must
 * hold all relevant data pertaining the host: its
 * private and public cookie, its nickname, when
 * it connected etc. etc..
 * </p>
 * 
 * <p>
 * Since multiple threads share Sessions, the access
 * is of course synchronized to all methods.
 * </p>
 * 
 * @author Alessio
 *
 */
public class Session {

	private String privateCookie;
	private String publicCookie;
	private String hostNickname;
	private String hostData;
	private boolean claimed = false;
	private boolean valid = true;	//As long as this flag is up, the session can be used
	private long start;
	private long hostTimeout;	//Milliseconds
	private Socket hostSocket;
	private Socket guestSocket;
	
	public Session (String hostNickname, long hostTimeout, String hostData) {
		this.hostNickname = hostNickname;
		this.hostTimeout = hostTimeout;
		this.hostData = hostData;
		
		privateCookie = UUID.randomUUID().toString();
		publicCookie = UUID.randomUUID().toString();
		start = System.currentTimeMillis();
	}

	synchronized public String getPrivateCookie() {
		return privateCookie;
	}

	synchronized public String getPublicCookie() {
		return publicCookie;
	}

	synchronized public String getHostNickname() {
		return hostNickname;
	}

	synchronized public long getHostTimeout() {
		return hostTimeout;
	}

	synchronized public String getHostData() {
		return hostData;
	}
	
	synchronized public Socket getHostSocket() {
		return hostSocket;
	}

	synchronized public Socket getGuestSocket() {
		return guestSocket;
	}

	synchronized public boolean expired () {
		return (System.currentTimeMillis() - start) > hostTimeout; 
	}
	
	/**
	 * Tries to claim the host. If the host has already been claimed, returns false.
	 * 
	 * @return
	 */
	synchronized public boolean claim (Socket guestSocket) {
		if (!claimed) {
			claimed = true;
			this.guestSocket = guestSocket;
			return true;
		} else return false;
	}
	
	/**
	 * Tries to unclaim the session, in a clean manner.
	 * If a hostSocket is found then it must be warned and the session invalidated.
	 * This will also close the guest socket.
	 * 
	 * @return true if the session was unclaimed, false if it was invalidated
	 */
	synchronized public boolean unclaim () {
		
		if (guestSocket != null) {
			try {
				if (!guestSocket.isClosed()) guestSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//The host hasn't been paired yet, we can unclaim this session.
		if (hostSocket == null) {
			claimed = false;
			guestSocket = null;
			return true;
		} 
		
		//The host is already waiting for the finalization, this session must be invalidated.
		else {
			
			//Notify the host
			try {
				new PrintWriter(hostSocket.getOutputStream(), true).println("REFUSED GUEST_CLOSED");
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				
				try {
					if (!hostSocket.isClosed()) hostSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				//Remove the session.
				SGPPServer.remove(this);
			}
			
			return false;
		}
	}
	
	synchronized public boolean isClaimed() {
		return claimed;
	}
	
	/**
	 * Pairs a guest claimed session to its host given its socket. 
	 * 
	 * @param hostSocket
	 * @return true for successful pairing, false if the session
	 * was not claimed.
	 */
	synchronized public boolean pair (Socket hostSocket) {
		if (claimed) {
			this.hostSocket = hostSocket;
			return true;
		} else {
			return false;
		}
	}

	synchronized public void invalidate () {
		valid = false;
	}
	
	synchronized public boolean isValid () {
		return valid;
	}
}
