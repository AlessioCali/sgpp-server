package com.gff.sgppserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The main executable of the thread. It allocates resources
 * then accepts incoming requests. Whenever a new connection
 * is accepted, it's dispatched to a {@link Handle} thread. In the
 * background, a {@link CleanupThread} removes any expired
 * connection while multiple {@link BridgeService}s perform
 * IO bridging between paired hosts and guests.
 * 
 * @author Alessio
 *
 */
public class SGPPServer {

	private static final int PORT = 8400;
	private static final int POOL_SIZE = 100;
	private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(POOL_SIZE);
	
	public static final ConcurrentHashMap<String, Session> PUBLIC_MAP = new ConcurrentHashMap<String, Session>();
	public static final ConcurrentHashMap<String, Session> PRIVATE_MAP = new ConcurrentHashMap<String, Session>();
	
	public static void main(String[] args) {
		
		ServerSocket server = null;
		
		try {
			server = new ServerSocket(PORT);
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return;
		}
		
		new CleanupThread().start();
		
		System.out.println("================================");
		System.out.println("*   SGPP MATCHMAKING SERVER    *");
		System.out.println("*            v0.2              *");
		System.out.println("================================");
		
		while(true) {
			if (server != null) {
				try {
					Socket s = server.accept();
					System.out.println("Received request from: " + s.getRemoteSocketAddress());
					EXECUTOR.submit(new Handle(s));
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else break;
		}	
		
		try {
			server.close();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}

	/**
	 * Allows submission of a new job without giving full access
	 * to the executor.
	 * 
	 * @param r
	 */
	public static void execute(Runnable r) {
		EXECUTOR.submit(r);
	}
	
	/**
	 * Removes and invalidate a session from the public
	 * and private maps.
	 * 
	 * @param s
	 */
	public static void remove (Session s) {
		PUBLIC_MAP.remove(s.getPublicCookie());
		PRIVATE_MAP.remove(s.getPrivateCookie());
		s.invalidate();
	}
	
	/**
	 * Performs the actual bridging of a paired Host
	 * and Guest.
	 * 
	 * @param s
	 */
	public static void bridge (Session s) {
		remove(s);
		EXECUTOR.submit(new BridgeService(s.getGuestSocket(), s.getHostSocket()));
		EXECUTOR.submit(new BridgeService(s.getHostSocket(), s.getGuestSocket()));
	}
}
