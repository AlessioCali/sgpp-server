package com.gff.sgppserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * Java Runnable that finalizes a connecting guest to its 
 * host. This means it will handle any CLOSE or STATUS
 * request from the connecting guest while waiting for the host,
 * and perform the final handshake before bridging the two.
 * 
 * @author Alessio
 *
 */
public class Finalizer implements Runnable {
	
	private Session session;
	private BufferedReader guestReader;
	private PrintWriter guestWriter;
	private PrintWriter hostWriter;
	private boolean shouldUnclaim = true;
	
	public Finalizer (Session session) {
		this.session = session;
	}

	@Override
	public void run () {
		try {
			guestReader = new BufferedReader (new InputStreamReader(session.getGuestSocket().getInputStream()));
			guestWriter = new PrintWriter(session.getGuestSocket().getOutputStream(), true);
			
			String query;

			//Cycle as long as the session is valid.
			while (session.isValid()) {
				query = guestReader.readLine();
				
				System.out.println(session.getGuestSocket().getRemoteSocketAddress() + " - " + query);
				
				//The guest closed its connection, we can break.
				if (query == null) break;
				
				else if (query.equals("STATUS")) {
					
					if (session.getHostSocket() != null) {

						//We have a Host! Tell the guest it's matched and ask its answer.
						hostWriter = new PrintWriter(session.getHostSocket().getOutputStream(), true);
						guestWriter.println("MATCHED");
						query = guestReader.readLine();
						
						System.out.println(session.getHostSocket().getRemoteSocketAddress() + " - " + query);
						
						//Guest closed, go away.
						if (query == null) break;
						
						//Oh god thanks the ordeal is over. Send final ack.
						else if (query.equals("READY")) {
							guestWriter.println("GAME_READY");
							hostWriter.println("GAME_READY");
							SGPPServer.bridge(session);
							shouldUnclaim = false;
							return;	//Only in this case we won't unclaim the session.
						} 
						
						else break;
					} 
					
					else guestWriter.println("WAITING");			
				} 
				
				//The query was not a STATUS, probably a close. Unclaim.
				else break;
			}
			
			//If the session has been invalidated the guest might be waiting. Send HOST_CLOSED
			guestWriter.println("REFUSED HOST_CLOSED");
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (shouldUnclaim) session.unclaim();
		}
	}
}
