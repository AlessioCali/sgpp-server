package com.gff.sgppserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * This Runnable bridges two connections: an incoming connection is
 * read and written on an outgoing connection.
 * 
 * @author Alessio
 *
 */
public class BridgeService implements Runnable {

	private final static int TIMEOUT = 60000;
	
	private Socket from;
	private Socket to;

	private BufferedReader reader;
	private PrintWriter writer;

	public BridgeService (Socket from, Socket to) {
		this.from = from;
		this.to = to;
	}

	@Override
	public void run() {
		try {
			from.setSoTimeout(TIMEOUT);
			reader = new BufferedReader(new InputStreamReader(from.getInputStream()));
			writer = new PrintWriter(to.getOutputStream(), true);

			String msg;

			while (true) {
				msg = reader.readLine();
				if (msg == null) msg = "CLOSE";
				if (!to.isClosed()) writer.println(msg);
				if (msg.equals("CLOSE")) break;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			
			System.out.println("User closed. Stopping connection from " + from.getRemoteSocketAddress());
			
			try {
				from.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		}
	}

}
