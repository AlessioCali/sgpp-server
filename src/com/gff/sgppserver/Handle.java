package com.gff.sgppserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class Handle implements Runnable {

	private static final String MSG_TIMEOUT = "Socket timeout";
	private static final int SO_TIMEOUT = 10000;
	private static final long TIMEOUT_LIMIT = 300000;
	
	//Possible queries
	private static final String QUERY_LIST = "LIST";
	private static final String QUERY_REGISTER = "REGISTER";
	private static final String QUERY_CONNECT = "CONNECT";
	private static final String QUERY_CLOSE = "CLOSE";
	private static final String QUERY_STATUS = "STATUS";
	private static final String QUERY_READY = "READY";
	
	//Answers
	private static final String ANS_BEGIN_LIST = "LIST_BEGIN";
	private static final String ANS_END_LIST = "LIST_END";
	private static final String ANS_OK = "OK ";
	
	//Errors
	private static final String FAIL_INVALID_QUERY = "REFUSED INVALID_QUERY";
	private static final String FAIL_NO_HOST = "REFUSED NO_HOST";
	private static final String FAIL_CLAIMED = "REFUSED HOST_CLAIMED";
	private static final String FAIL_COOKIE = "REFUSED INVALID_COOKIE";
	private static final String FAIL_GUEST_CLOSED = "REFUSED GUEST_CLOSED";
	
	private Socket socket;
	private BufferedReader reader;
	private PrintWriter writer;
	private String query;
	private boolean shouldDispose = true;
	
	public Handle(Socket socket) {
		this.socket = socket;
	}
	
	@Override
	public void run() {
		try { 
			socket.setSoTimeout(SO_TIMEOUT);
			reader = new BufferedReader (new InputStreamReader (socket.getInputStream()));
			writer = new PrintWriter(socket.getOutputStream(), true);
			
			query = reader.readLine();
			
			System.out.println(socket.getRemoteSocketAddress() + " - " + query);
			
			if (query == null) {
				dispose();
				return;
			} else if (query.equals(QUERY_LIST)) {
				handleList();
			} else if (query.startsWith(QUERY_REGISTER)) {
				handleRegister();
			} else if (query.startsWith(QUERY_CLOSE)) {
				handleClose();
			} else if (query.startsWith(QUERY_CONNECT)) {
				shouldDispose = !handleConnect();
			} else if (query.startsWith(QUERY_STATUS)) {
				shouldDispose = !handleStatus();	//Do not dispose if status matched
			}
		} catch (SocketTimeoutException soe) {
			System.out.println(MSG_TIMEOUT);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (NumberFormatException nfe) {
			writer.println(FAIL_INVALID_QUERY);
		} finally {
			dispose();
		}
	}
	
	/**
	 * Queries containing a CLOSE message come from Host
	 * who are no longer connected. The CLOSE coming from Guests
	 * is handled separately on an already open connection.
	 */
	private void handleClose() {
		String[] args = query.split("\\s", 2);
		
		if (args.length < 2) writer.println(FAIL_INVALID_QUERY);
		
		else {
			Session s = SGPPServer.PRIVATE_MAP.get(args[1]);
			
			if (s != null)
				SGPPServer.remove(s);
		}
	}

	/**
	 * Prints the list of all hosts with their public cookies. The format is:<br>
	 * NICKNAME COOKIE DATA
	 */
	private void handleList() {
		writer.println(ANS_BEGIN_LIST);
		
		for (Session s : SGPPServer.PUBLIC_MAP.values())
			writer.println(String.format("%s %s %s", s.getHostNickname(), s.getPublicCookie(), s.getHostData()));				
		
		writer.println(ANS_END_LIST);
	}
	
	/**
	 * Handles registration of a new host. All info
	 * are extracted from the query and a new entry is
	 * created. After that the private cookie is sent back
	 * to the new host.
	 */
	private void handleRegister() {
		String[] hostInfo = query.split("\\s", 4);
		
		if (hostInfo.length < 4) {
			writer.println(FAIL_INVALID_QUERY);
		} 
		
		else {
			String nickname = hostInfo[1];
			long timeout = Long.valueOf(hostInfo[2]);
			String data = hostInfo[3];
			
			//The timeout is invalid, refuse.
			if (timeout < 0 || timeout > TIMEOUT_LIMIT) {
				writer.println(FAIL_INVALID_QUERY);
			} 
			
			else {
				Session s = new Session(nickname, timeout, data);
				SGPPServer.PUBLIC_MAP.put(s.getPublicCookie(), s);
				SGPPServer.PRIVATE_MAP.put(s.getPrivateCookie(), s);
				writer.println(ANS_OK + s.getPrivateCookie());
			}
		}
	}
	
	/**
	 * Handles a connection request from a guest. If all
	 * goes well the connection is handed to a Finalizer,
	 * otherwise an error message is sent and the connection
	 * is appropriately disposed of.
	 * 
	 * @return true if the connection was handled to Finalizer,
	 * false otherwise (it must be disposed).
	 */
	private boolean handleConnect () {
		String[] info = query.split("\\s", 2);
		
		if (info.length < 2) {
			writer.println(FAIL_INVALID_QUERY);
			return false;
		}
		
		else {
			String cookie = info[1];
			Session s = SGPPServer.PUBLIC_MAP.get(cookie);
			
			if (s != null) {
				
				//We claimed the host, now handle to Finalizer.
				if (s.claim(socket)) {
					writer.println(ANS_OK);
					SGPPServer.execute(new Finalizer(s));
					return true;
				} 
				
				else {
					writer.println(FAIL_CLAIMED);
					return false;
				}
				
			} 
			
			else {
				writer.println(FAIL_NO_HOST);
				return false;
			}
		}
	}
	
	/**
	 * Handles a status request from a host. Status
	 * requests from guests are handled inside Finalizer.
	 * If the query is invalid or the host has not been
	 * matched yet, the connection is disposed. Otherwise
	 * its disposal depends on the result of 
	 * {@link #handleMatchedHost(Session)}.
	 * 
	 * @return false for invalid query or still waiting,
	 * the result of {@link #handleMatchedHost(Session)}
	 * otherwise.
	 */
	private boolean handleStatus() {
		String[] info = query.split("\\s", 2);
		
		if (info.length < 2) {
			writer.println(FAIL_INVALID_QUERY);
			return false;
		} 
		
		else {
			String cookie = info[1];
			Session s = SGPPServer.PRIVATE_MAP.get(cookie);
			
			if (s != null) {
				
				//There's a match, handle it.
				if (s.isClaimed()) {
					return handleMatchedHost(s);
				} 
				
				else {
					writer.println("WAITING");
					return false;
				}
			} 
			
			else {
				writer.println(FAIL_COOKIE);
				return false;
			}
		}
	}
	
	private boolean handleMatchedHost(Session s) {
		writer.println("MATCHED");
		
		try {
			query = reader.readLine();
		} catch (Exception e) {
			return false;
		}
		
		if (query.equals(QUERY_READY)) {
			
			//After the pairing any further interaction is handled in Finalizer.
			if (s.pair(socket)) return true;
			
			//The session was supposed to be claimed, but it is no more.
			else {
				writer.println(FAIL_GUEST_CLOSED);
				return false;
			}
			
		} else {
			return false;
		}
	}

	private void dispose() {
		if (socket != null && shouldDispose) {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
