package com.gff.sgppserver;

/**
 * This threads looks for expired sessions and removes.
 * The lookup is done every 5 seconds.
 * 
 * @author Alessio
 *
 */
public class CleanupThread extends Thread {

	@Override
	public void run () {	
		while (true) {
			for (Session s : SGPPServer.PUBLIC_MAP.values())
				if (s.expired()) SGPPServer.remove(s);
			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				//I don't care.
			}
		}
	}
}
